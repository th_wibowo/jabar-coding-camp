export default {
  namespaced: true,
  state: {
    count: 0
  },
  getters: {
    count : (state) => {
      return state.count
    }
  },
  mutations: {//payload adalah variabel untuk menentukan besar increment
    increment : (state, payload) => {
      state.count += payload
    }
  },
  actions: {

  }
}
