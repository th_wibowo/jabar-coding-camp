//================================ SOAL 1 =====================================
  //pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
  /*nilai >= 85 indeksnya A
    nilai >= 75 dan nilai < 85 indeksnya B
    nilai >= 65 dan nilai < 75 indeksnya c
    nilai >= 55 dan nilai < 65 indeksnya D
    nilai < 55 indeksnya E
  */
  // JAWABAN SOAL 1

    var nilai = 88;

    console.log("================== SOAL 1 ================== \n")
    if(nilai >= 85){
      console.log("Indeksnya A");
    }
    else if((nilai >= 75) && (nilai < 85)){
      console.log("Indeksnya B");
    }
    else if((nilai >= 65) && (nilai < 75)){
      console.log("Indeksnya C");
    }
    else if((nilai >= 55) && (nilai < 65)){
      console.log("Indeksnya D");
    }
    else if(nilai < 55){
      console.log("Indeksnya E");
    }

////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 2 =====================================
  /*buatlah variabel seperti di bawah ini
    var tanggal = 22;
    var bulan = 7;
    var tahun = 2020;
    ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan,
    lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)
  */
  // JAWABAN SOAL 2

    var tanggal = 8;
    var bulan = 8;
    var tahun = 1994;
    var namaBulan = '';

    switch(bulan) {
        case 1:   { namaBulan = 'Januari'; break; }
        case 2:   { namaBulan = 'Februari'; break; }
        case 3:   { namaBulan = 'Maret'; break; }
        case 4:   { namaBulan = 'April'; break; }
        case 5:   { namaBulan = 'Mei'; break; }
        case 6:   { namaBulan = 'Juni'; break; }
        case 7:   { namaBulan = 'Juli'; break; }
        case 8:   { namaBulan = 'Agustus'; break; }
        case 9:   { namaBulan = 'September'; break; }
        case 10:   { namaBulan = 'Oktober'; break; }
        case 11:   { namaBulan = 'November'; break; }
        case 12:   { namaBulan = 'Desember'; break; }
        default:  { console.log('Please input month number'); namaBulan = bulan; }
    }
    console.log("\n================== SOAL 2 ================== \n")
    console.log(tanggal + " " + namaBulan + " " + tahun);

////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 3 =====================================
  /*Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).
          Output untuk n=3 :
          #
          ##
          ###
  */
  // JAWABAN SOAL 3

    var n = 7;
    var output = "";
    console.log("\n================== SOAL 3 ================== \n")
    /*for (i = 1; i <= n; i++){
        for (j = 1; j <= i; j++){
          output += "#";
        }
        console.log(output);
        output = "";
      }*/
    //atau dengan sekali loop
    for (i = 1; i <= n; i++){
      output += "#";
      console.log(output);
    }

////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 4 =====================================
  /*berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
    contoh : Output untuk m = 7
                    1 - I love programming
                    2 - I love Javascript
                    3 - I love VueJS
                    ===
                    4 - I love programming  *)berulang ke kalimat pertama setelah m = 4
                    5 - I love Javascript
                    6 - I love VueJS
                    ======                  *)jumlah garis pembatas bertambah sesuai dengan nilai m yg terakhir
                    7 - I love programming
  */
  // JAWABAN SOAL 4

    var i = 1
    var m = 10
    var output = "";
    var word = "";
    var substring = " - I love ";
    console.log("\n================== SOAL 4 ================== \n")

    while(i <= m){
      switch(i % 3) {
          case 1:   { word = 'programming'; break; }
          case 2:   { word = 'Javascript'; break; }
          case 0:   { word = 'VueJS'; break; }
          default:  { console.log('none'); word = "none"; }
      }
      output = i + substring + word;
      console.log(output);
      if (i % 3 == 0){
        var barrier = "";
        for (j = 1; j <= i; j++){
          barrier += "=";
        }
        console.log(barrier);
      }
      i++;
    }

////////////////////////////////////////////////////////////////////////////////
