//================================ SOAL 1 =====================================
  /*buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
  */
  // JAWABAN SOAL 1

    const luasPersegiPanjang = (panjang, lebar) => {
      let x = panjang;
      let y = lebar;
      return x * y;
    }

    const kelilingPersegiPanjang = (panjang, lebar) => {
      let x = panjang;
      let y = lebar;
      return 2 * (x + y);
    }

    console.log("================== SOAL 1 ================== \n");
    console.log("Luas: " + luasPersegiPanjang(35, 14));
    console.log("Keliling: " + kelilingPersegiPanjang(35, 14));

////////////////////////////////////////////////////////////////////////////////
//================================ SOAL 2 =====================================
  /*Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana:
          const newFunction = function literal(firstName, lastName){
            return {
              firstName: firstName,
              lastName: lastName,
              fullName: function(){
                console.log(firstName + " " + lastName)
              }
            }
          }
          //Driver Code
          newFunction("William", "Imoh").fullName()
  */
  // JAWABAN SOAL 2

    const newFunction = (...rest) => {//bisa tanpa rest
      let [firstName, lastName] = rest;
      return{
        fullName : (rest)=> {
          console.log(`${firstName} ${lastName}`);
        }
      }
    };

    console.log("\n================== SOAL 2 ================== \n");
    //Driver Code
    newFunction("William", "Imoh").fullName()

////////////////////////////////////////////////////////////////////////////////
//================================ SOAL 3 =====================================
  //Diberikan sebuah objek sebagai berikut:
          const newObject = {
            firstName: "Muhammad",
            lastName: "Iqbal Mubarok",
            address: "Jalan Ranamanyar",
            hobby: "playing football",
          }
  /*dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
            const firstName = newObject.firstName;
            const lastName = newObject.lastName;
            const address = newObject.address;
            const hobby = newObject.hobby;
    Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
  */
  // JAWABAN SOAL 3

    const {firstName, lastName, address, hobby} = newObject;
    // Driver code
    console.log("\n================== SOAL 3 ================== \n");
    console.log(firstName, lastName, address, hobby);

////////////////////////////////////////////////////////////////////////////////
//================================ SOAL 4 =====================================
  //Kombinasikan dua array berikut menggunakan array spreading ES6:

    const west = ["Will", "Chris", "Sam", "Holly"];
    const east = ["Gill", "Brian", "Noel", "Maggie"];

  // JAWABAN SOAL 4

    //const combined = west.concat(east);
    let combined = [...west, ...east];
    //Driver Code
    console.log("\n================== SOAL 4 ================== \n");
    console.log(combined);

////////////////////////////////////////////////////////////////////////////////
//================================ SOAL 5 =====================================
  /*sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
    const planet = "earth"
    const view = "glass"
    var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
  */
  // JAWABAN SOAL 5

    const planet = "earth";
    const view = "glass";
    const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;

    console.log("\n================== SOAL 5 ================== \n");
    console.log(after);

////////////////////////////////////////////////////////////////////////////////
