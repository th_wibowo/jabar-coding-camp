// SOAL 1

  var pertama = "saya sangat senang hari ini";
  var kedua = "belajar javascript itu keren";
  //gabungkan variabel-variabel tersebut agar menghasilkan output

// JAWABAN SOAL 1

  var string1 = pertama.substring(0, 4);
  var string2 = pertama.substr(12, 6);
  var string3 = kedua.substring(0, 7);
  var string4 = kedua.substr(8, 10).toUpperCase();

  var sentence = string1 + " " + string2 + " " + string3 + " " + string4;
  console.log("===== SOAL 1 ===== \n")
  console.log(sentence + "\n");

////////////////////////////////////////////////////////////////////////////////

// SOAL 2

  var kataPertama = "10";
  var kataKedua = "2";
  var kataKetiga = "4";
  var kataKeempat = "6";
  //ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer) dgn 3 operasi saja

// JAWABAN SOAL 2

  var num1 = Number(kataPertama);
  var num2 = Number(kataKedua);
  var num3 = parseInt(kataKetiga);
  var num4 = parseInt(kataKeempat);

  var newnumber = num3 * num4 + (num1 % num2);
  console.log("===== SOAL 2 ===== \n")
  console.log("Hasil setelah dioperasikan: " + newnumber + "\n");

////////////////////////////////////////////////////////////////////////////////

// SOAL 3

  var kalimat = 'wah javascript itu keren sekali';

  var kataPertama = kalimat.substring(0, 3);
  var kataKedua = kalimat.substring(4, 14); // do your own!
  var kataKetiga = kalimat.substring(15, 18); // do your own!
  var kataKeempat = kalimat.substring(19, 24); // do your own!
  var kataKelima = kalimat.substring(25); // do your own!

  console.log("===== SOAL 3 ===== \n")
  console.log('Kata Pertama: ' + kataPertama);
  console.log('Kata Kedua: ' + kataKedua);
  console.log('Kata Ketiga: ' + kataKetiga);
  console.log('Kata Keempat: ' + kataKeempat);
  console.log('Kata Kelima: ' + kataKelima);
