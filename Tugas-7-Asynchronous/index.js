var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Tulis code untuk memanggil function readBooks di sini

function execute(time, books, index){//value parameter harus sama
    readBooks(time, books[index], function(waktuTersisa){
      index++;
      if ((waktuTersisa !== 0) && (index !== books.length)){
        execute(waktuTersisa, books, index);//value parameter harus sama
      }
      else if((waktuTersisa !== 0) && (index === books.length)){
        console.log('sudah tidak ada buku yang bisa dibaca');
      }
      else{
        console.log('waktu saya habis');
      }
    })
}

execute(15000, books, 0);
