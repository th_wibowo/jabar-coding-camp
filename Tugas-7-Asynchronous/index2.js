var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise

function execute(time, books, index){//value parameter harus sama
    readBooksPromise(time, books[index])
      .then(function(waktuTersisa){
        index++;
        if ((index !== books.length) && (waktuTersisa !== 0)){
          execute(waktuTersisa, books, index);//value parameter harus sama
        }
        else if((index === books.length) && (waktuTersisa !== 0)){
          console.log('sudah tidak ada buku yang bisa dibaca');
        }
        else{
          console.log('waktu saya habis');
        }
      })
      .catch(function(error){
        console.log(error);
      })
}

execute(15000, books, 0);
