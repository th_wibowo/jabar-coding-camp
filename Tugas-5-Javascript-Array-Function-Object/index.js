//================================ SOAL 1 =====================================
  /*buatlah variabel seperti di bawah ini:
    var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
    urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
          1. Tokek
          2. Komodo
          3. Cicak
          4. Ular
          5. Buaya
  */
  // JAWABAN SOAL 1

    var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
    console.log("================== SOAL 1 ================== \n");

    daftarHewan.sort();
    //menggunakan for()
    for (i = 0; i < daftarHewan.length; i++){
      console.log(daftarHewan[i]);
    }
    //atau menggunakan foreach()
    /*daftarHewan.forEach(function(item){
      console.log(item);
    })*/

////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 2 =====================================
  /*Tulislah sebuah function dengan nama introduce() yang memproses paramater
    yang dikirim menjadi sebuah kalimat perkenalan seperti berikut:
    "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]" !
  */
  // JAWABAN SOAL 2

    function introduce(data){
      var sentence = "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
      return sentence;
    }

    var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

    var perkenalan = introduce(data);

    console.log("\n================== SOAL 2 ================== \n");
    console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 3 =====================================
  /*Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string,
    kemudian memproses paramater tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
  */
  // JAWABAN SOAL 3

    function hitung_huruf_vokal(kata){
      var count = 0;
      var vocal = ["a", "i", "u", "e", "o", "A", "I", "U", "E", "O"];
      var letterArray = kata.split(''); //memecah string menjadi elemen2 array
      letterArray.forEach(function(huruf){
        vocal.forEach(function(hurufVokal){
          if ( hurufVokal == huruf ){
            count += 1;
          }
        })
      })
      return count;
    }

    var hitung_1 = hitung_huruf_vokal("Muhammad");

    var hitung_2 = hitung_huruf_vokal("Iqbal");

    console.log("\n================== SOAL 3 ================== \n");
    console.log("total huruf vokal: " + hitung_1 + " dan " + hitung_2); // 3 2


////////////////////////////////////////////////////////////////////////////////

//================================ SOAL 4 =====================================
  /*Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer
    dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut:
        console.log( hitung(0) ) // -2
        console.log( hitung(1) ) // 0
        console.log( hitung(2) ) // 2
        console.log( hitung(3) ) // 4
        console.log( hitung(5) ) // 8
  */
  // JAWABAN SOAL 4

    function hitung(integer){
      var a = -2;
      var b = -a;
      var count = a + (b * integer);
      return count;
    }

    console.log("\n================== SOAL 4 ================== \n");
    console.log( hitung(0) );
    console.log( hitung(1) );
    console.log( hitung(2) );
    console.log( hitung(3) );
    console.log( hitung(5) );

////////////////////////////////////////////////////////////////////////////////
