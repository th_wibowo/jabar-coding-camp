//================================ SOAL 1 =====================================
  /*Function Penghasil Tanggal Hari Esok
      Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.

      contoh 1

      var tanggal = 29
      var bulan = 2
      var tahun = 2020

      next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

      contoh 2

      var tanggal = 28
      var bulan = 2
      var tahun = 2021

      next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

      contoh 3

      var tanggal = 31
      var bulan = 12
      var tahun = 2020

      next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

      Catatan :
      1. Dilarang menggunakan new Date()
      2. Perhatikan tahun kabisat
  */
  // JAWABAN SOAL 1
    function next_date(day, month, year){
      var daftarbulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
      var tanggal = 0;
      var bulan = "";
      var tahun = 0;

        if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10)){
          if (day == 31){
            day = 1;
            month += 1;
          }
          else{
            day += 1;
          }
        }
        else if ((month == 4) || (month == 6) || (month == 9) || (month == 11)){
          if (day == 30){
            day = 1;
            month += 1;
          }
          else{
            day += 1;
          }
        }
        else if (month == 2){
          if (year % 4 == 0){
            if (day == 29){
              day = 1;
              month += 1;
            }
            else{
              day += 1;
            }
          }
          else{
            if (day == 28){
              day = 1;
              month += 1;
            }
            else{
              day += 1;
            }
          }

        }
        else if (month == 12){
          if (day == 31){
            day = 1;
            month = 1;
            year += 1;
          }
          else{
            day += 1;
          }
        }

      tanggal = day;
      bulan = daftarbulan[month-1];
      tahun = year;
      console.log(tanggal + " " + bulan + " " + tahun);
    }


    console.log("================== SOAL 2 ================== \n");

    var tanggal = 29
    var bulan = 2
    var tahun = 2020
    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

    var tanggal = 28
    var bulan = 2
    var tahun = 2021
    next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

    var tanggal = 31
    var bulan = 12
    var tahun = 2020
    next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

//////////////////////////////////////////////////////////////////////////////////
//================================ SOAL 2 ======================================
  /*Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.
    Contoh:
      var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
      var kalimat_2 = " Saya Iqbal"
      var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

      jumlah_kata(kalimat_1) // 6
      jumlah_kata(kalimat_2) // 2
      jumlah_kata(kalimat_3) // 4

      *catatan
      Perhatikan double spasi di depan, belakang, maupun di tengah tengah kalimat
  */
  // JAWABAN SOAL 2
    function jumlah_kata(string){
      //var totalLetter = "" ;
      var jumlahKata = 0;
      var hurufDariString = string.split('');
      var daftarAbjad = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
      for(i = 0; i < hurufDariString.length; i++){
        daftarAbjad.forEach(function(abjad){
          if ((i > 0) && (hurufDariString[i] == abjad) && (hurufDariString[i+1] == " "))
          {
              jumlahKata += 1;
          }
        })
      }

      console.log(jumlahKata);
    }

    var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
    var kalimat_2 = " Saya Iqbal"
    var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

    console.log("\n================== SOAL 2 ================== \n");

    jumlah_kata(kalimat_1) // 6
    jumlah_kata(kalimat_2) // 2
    jumlah_kata(kalimat_3) // 4

////////////////////////////////////////////////////////////////////////////////
